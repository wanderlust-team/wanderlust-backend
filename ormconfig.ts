import { TypeOrmModuleOptions } from "@nestjs/typeorm";
import { config } from 'dotenv';
import { Request } from "./src/modules/request/entities/request.entity";
import { Place } from "./src/modules/places/entities/place.entity";
import { Users } from "./src/modules/users/entities/user.entity";

config();

const { DB_USER_NAME, DB_PASSWORD, DB_NAME, HOST, NODE_ENV, DB_TEST } = process.env;

const ormconfig: TypeOrmModuleOptions = {
    type: 'postgres',
        host: HOST,
        port: 5432,
        username: DB_USER_NAME,
        password: DB_PASSWORD,
        database: NODE_ENV === 'test' ? DB_TEST : DB_NAME,
        entities: [Users, Place, Request],
        synchronize: NODE_ENV !== 'production',
        migrations: ['dist/src/db/migrations/*.js'],
        cli: {
            migrationsDir: 'src/db/migrations',
        },
        ssl:
            NODE_ENV === 'production'
                ? {
                    rejectUnauthorized: false,
                }
                : false,

        autoLoadEntities: true,
        logging: NODE_ENV !== 'production' && NODE_ENV !== 'test',
}

export default ormconfig;