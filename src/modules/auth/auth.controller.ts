import { UserDTO } from '../users/dto/create-user.dto';
import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ApiBadRequestResponse, ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { LoginDTO } from '../users/dto/login.dto';
import { AdminDTO } from '../users/dto/admin.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService){}
        
        @ApiCreatedResponse({description: 'Successfully registered'})
        @ApiBadRequestResponse({ description: 'Bad Request'})
        @ApiInternalServerErrorResponse({ description: 'Internal server error' })
        @Post('signup')
        newUser(@Body() user: UserDTO){
            return this.authService.register(user)
        }

        @ApiCreatedResponse({description: 'Successfully registered'})
        @ApiBadRequestResponse({ description: 'Bad Request'})
        @ApiInternalServerErrorResponse({ description: 'Internal server error' })
        @Post('signup/admin/secure')
        newAdmin(@Body() admin: AdminDTO){
            return this.authService.register(admin)
        } 

        @HttpCode(HttpStatus.OK)
        @ApiOkResponse({description: 'Successfully logged in'})
        @ApiUnauthorizedResponse({description: 'Something wrong with your token'})
        @ApiBadRequestResponse({ description: 'Bad Request'})
        @ApiInternalServerErrorResponse({ description: 'Internal server error' })
        @Post('login')
        async login(@Body() loginUserDto: LoginDTO) {
            return await this.authService.login(loginUserDto);  
        }
}