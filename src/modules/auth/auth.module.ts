import { AuthController } from './auth.controller';
import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsersModule } from '../users/users.module';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './jwt.strategy';
import {config} from 'dotenv'

config()


@Module({
  imports: [
    UsersModule,    
    PassportModule.register({
            defaultStrategy: 'jwt',
            property: 'user',
            session: false,
    }),
    JwtModule.register({
        secret: process.env.SECRETKEY, signOptions: {
        expiresIn: process.env.EXPIRESIN,
        },
    }),
  ],
  providers: [AuthService, JwtStrategy],
  controllers: [AuthController],
  exports: [PassportModule, JwtModule],
})
export class AuthModule {}
