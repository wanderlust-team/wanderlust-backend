import { role } from "../users/types";

export interface JwtPayload { 
    id: number, 
    email: string;
    firstname?: string;
    lastName: string;
    role: role
}