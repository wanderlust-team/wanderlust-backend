import { UserDTO } from '../users/dto/create-user.dto';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { Response } from '../../shared/interfaces/response.interface';
import { LoginDTO } from '../users/dto/login.dto';
import { JwtPayload } from './payload.interface';
import { AdminDTO } from '../users/dto/admin.dto';

@Injectable()
/**class representing AuthService */
export class AuthService {
  constructor(private usersService: UsersService, private readonly jwtService: JwtService,) {}
   
  /**
   * @description registers user in the system
   * @param createUserDto the reqBody sent by the user
   * @returns message and user 
   */
  async register(createUserDto: UserDTO | AdminDTO): Promise<Response>{
    const user = await this.usersService.register(createUserDto);
    return {
      message: 'user successfully created',
      data: user,
  }
  }

    /**
   * @description logins the user into the system
   * @param loginUserDto the credentials entered by the user in the body
   * @returns message and token
   */
     async login(loginUserDto: LoginDTO): Promise<Response> {     
      const user = await this.usersService.findByLogin(loginUserDto);
      const token = this._createToken({user});
      
      return {
          message: 'Successfully Logged in!',
          data: token,
      };  
    }
  
    /**
     * @description creates a token for the user
     * @param user users object used to create a token
     * @returns a token and it's expiration date 
     */
    private _createToken(user): any {
      const info: JwtPayload = user;    
      const accessToken = this.jwtService.sign(info);    
      return {
          expiresIn: process.env.EXPIRESIN,
          accessToken,    
      };  
    }

      /**
   * @description validates the payload entered by the user
   * @param payload the payload passed from jwt service strategy method
   * @returns user
   */
  async validateUser(payload: JwtPayload): Promise<UserDTO> {
    const user = await this.usersService.findByPayload(payload);
    if (!user) throw new UnauthorizedException()   
    return user;  
}

}

