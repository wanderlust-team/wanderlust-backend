import { Place } from '../entities/place.entity';

export class PlaceResponse {
  message: string;
  page: number;
  itemsPerPage: number;
  totalCount: number;
  data: Place[];
}
