import {IsNotEmpty, IsNumber, IsString, MinLength } from "class-validator";

export class PlaceDTO {

    @IsString()
    @IsNotEmpty()
    @MinLength(5, {
        message: 'Title should contain atleast 5 characters'
    })
    readonly title: string;

    @IsString()
    @IsNotEmpty()
    @MinLength(10, {
        message: 'Description should contain atleast 10 characters'
    })
    readonly description: string;

    @IsNotEmpty()
    readonly price: number;
    
    picture: any;
}