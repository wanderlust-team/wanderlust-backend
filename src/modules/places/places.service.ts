import { ForbiddenException, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Response } from '../../shared/interfaces/response.interface';
import { Repository } from 'typeorm';
import { PlaceDTO } from './dto/create-place.dto';
import { Place } from './entities/place.entity';
import CloudinaryService from '../../shared/providers/cloudinary';
import { PaginationDto } from '../../shared/dto/pagination.dto';
import { PlaceResponse } from './dto/place-response.dto';


@Injectable()
export class PlacesService {
  constructor(
    @InjectRepository(Place)
    private placeRepo: Repository<Place>,
    private cloudService: CloudinaryService
  ) {}

  /**
   * @description creates places to visit
   * @createPlaceDto Place info
   * @files entry image of a house
   * @return response
   */
  async create(req: any, createPlaceDto: PlaceDTO, file): Promise<Response>{
    if(req.role !== 'travelAdmin') throw new ForbiddenException(); 
    const url = await this.cloudService.uplaodToCloud(file.path)
    createPlaceDto.picture = url;
    const place: Place = this.placeRepo.create({
      owner: req,
      ...createPlaceDto
    });
    const createdPlace = await this.placeRepo.save(place);
    return { 
      message: 'Place successfully Created', 
      data: createdPlace };
  }

    /**
   * @description view all available places 
   * @return response
   */

  async findAll(paginationDto: PaginationDto): Promise<PlaceResponse>{
    const skippedItems = (paginationDto.page - 1) * paginationDto.limit;
    const [places, totalCount] = await this.placeRepo.findAndCount({
      take: paginationDto.limit,
      skip: skippedItems,
    });

    if(!places.length) throw new NotFoundException('no places for paying visit for the moment');
    return {
      message: 'Successfully found all the places',
      page: paginationDto.page,
      itemsPerPage: paginationDto.limit,
      totalCount: totalCount,
      data: places
    }
  }

   /**
   * @description View a single place details
   * @param id id of the place to checked against the placeRepo
   * @returns Place object
   */
  
  async findPlace(id:number): Promise<Response>{
    const place = await this.placeRepo.findOne({id}, {
      relations: ['owner']
    });
    if(!place) throw new NotFoundException('place not found');
    delete place.owner.password;
    return {
      message: 'Successfully found the place',
      data: place
    }
  }
}
