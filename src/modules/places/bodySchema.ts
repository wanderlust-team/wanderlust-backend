export const placeSchema = {
    schema: {
        type: 'object',
        properties: {
          file: {
            type: 'string',
            format: 'binary',
          },
          title: {
            type: 'string',
            default: 'Kibuye Weekend Camping',
          },
          description: {
            type: 'string',
            default: "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
          },
          price: {
            type: 'number',
            default: '20000',
          },
        },
      },
}