import { Module } from '@nestjs/common';
import { PlacesService } from './places.service';
import { PlacesController } from './places.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Place } from './entities/place.entity';
import CloudinaryService from '../../shared/providers/cloudinary';

@Module({
  imports: [TypeOrmModule.forFeature([Place])],
  controllers: [PlacesController],
  providers: [
    PlacesService,
    {
      provide: CloudinaryService,
      useClass: CloudinaryService
    }
  ],
  exports: [PlacesService]
})
export class PlacesModule {}
