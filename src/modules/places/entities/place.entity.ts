import { Users } from "../../users/entities/user.entity"
import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm"
import { Request } from "../../request/entities/request.entity"

@Entity({name: 'places'})
export class Place {
    @PrimaryGeneratedColumn()
    id: number
    
    @Column({ nullable: false})
    title: string

    @Column({ nullable: false})
    description: string

    @Column({ nullable: false})
    price: number

    @Column()
    picture: string

    @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
    createdAt: Date;
  
    @UpdateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
    updatedAt: Date;
  
    @ManyToOne(() => Users, (user: Users) => user.places, {onDelete: 'CASCADE'})
    owner: Users;

    @OneToMany(()=> Request, (request: Request)=> request.place, {onDelete: 'CASCADE'})
    requests: Request[]
  
}
