import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, Req, UploadedFile, UseGuards, Query } from '@nestjs/common';
import { PlacesService } from './places.service';
import { PlaceDTO } from './dto/create-place.dto';
import { ApiBadRequestResponse, ApiBearerAuth, ApiBody, ApiConsumes, ApiCreatedResponse, ApiForbiddenResponse, ApiInternalServerErrorResponse, ApiNotFoundResponse, ApiOkResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { editFileName, imageFileFilter } from '../../utils/fileUpload.utils';
import { AuthGuard } from '@nestjs/passport';
import { placeSchema } from './bodySchema';
import { PaginationDto } from '../../shared/dto/pagination.dto';

@ApiTags('Place')
@ApiBearerAuth()
@Controller('places')
@UseGuards(AuthGuard('jwt'))
export class PlacesController {
  constructor(private readonly placesService: PlacesService) { }


  /**
 * Create a house for sale
 * @req request data
 * @body entry data
 * @files entry images of a house
 * @return response
 */
  @ApiConsumes('multipart/form-data')
  @ApiBody(placeSchema)
  @ApiCreatedResponse({ description: 'Successfully Created' })
  @ApiUnauthorizedResponse({ description: 'Login first' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  @ApiForbiddenResponse({ description: 'Forbiden' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: editFileName,
      }),
      fileFilter: imageFileFilter,
    }),
  )
  create(
    @Body() createPlaceDto: PlaceDTO,
    @Req() req: any,
    @UploadedFile() file: Express.Multer.File
  ) {
    return this.placesService.create(req.user, createPlaceDto, file);
  }

  @ApiOkResponse({ description: 'Successfully got all the places' })
  @ApiUnauthorizedResponse({ description: 'Login first' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Get()
  findPlaces(@Query() paginationDto: PaginationDto) {
    paginationDto.page = Number(paginationDto.page)
    paginationDto.limit= Number(paginationDto.limit)
    return this.placesService.findAll({...paginationDto, limit: paginationDto.limit> 10? 10: paginationDto.limit});
  }

  @ApiOkResponse({ description: 'Successfully found a todo' })
  @ApiUnauthorizedResponse({ description: 'Login first' })
  @ApiNotFoundResponse({ description: 'No Todo found' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Get(':id')
  findPlace(@Param('id') placeId: number) {
    return this.placesService.findPlace(placeId)
  }
}
