import { ForbiddenException, Injectable, UnauthorizedException } from '@nestjs/common';
import { Response } from '../../shared/interfaces/response.interface';
import { DeleteResult } from 'typeorm';
import { UsersService } from '../users/users.service';
import {PlaceQueryDto} from './placeQuery.dto'

@Injectable()
export class AdminService {
  constructor(private usersService: UsersService){}


    /**
   * @description this method finds all the users
   * @param conditions condition to be checked against userRepository
   * @returns users object
   */
 async findAll(req, {type}: PlaceQueryDto): Promise<Response>{
   if(req.role !== 'superAdmin') throw new ForbiddenException();
   const conditions = type === 'users' ? [{role : 'travelAdmin'},{role: 'traveller'}] : {role: 'travelAdmin'}
   const users = await this.usersService.findAllUsers(conditions);
   return {
     message: 'successfully found all users',
     data: users
   }
 }
 
  /**
   * @description this method updates the user role
   * @param conditions condition to be checked against userRepository
   * @returns users object
   */
 async updateRole(req, email: string, newRole: any): Promise<Response>{
  if(req.role !== 'superAdmin'){throw new ForbiddenException();} 
   const changeRole = await this.usersService.updateRole(email, newRole);
   return {
     message: 'You successfully updated the user',
     data: changeRole
   }
 }

   /**
 * @description deletes a user 
 * @param id user id
 * @returns user object
 */
 async deleteUser(req: any, id:number): Promise<DeleteResult>{
   if(req.role !== 'superAdmin') throw new ForbiddenException();
   const deleteUser = await this.usersService.deleteUser(id);
   return deleteUser;
 }
 
}
