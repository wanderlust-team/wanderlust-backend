import { IsEnum, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class PlaceQueryDto {
  @ApiProperty({
    enum: ['users', 'travelAdmin'],
    default: 'users',
  })
  @IsNotEmpty()
  @IsEnum(['users', 'travelAdmin'], {
    message: 'Type must be either users or travelAdmin',
  })
  readonly type: string;
}
