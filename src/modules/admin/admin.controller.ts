import { Controller, Get, Post, Body, Patch, Param, Delete, Query, UseGuards, Req } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBadRequestResponse, ApiBearerAuth, ApiCreatedResponse, ApiForbiddenResponse, ApiInternalServerErrorResponse, ApiOkResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { ChangeRoleDTO } from '../users/dto/update-role.dto';
import { AdminService } from './admin.service';
import { PlaceQueryDto } from './placeQuery.dto';

@ApiTags('Admin')
@ApiBearerAuth()
@Controller('admin')
@UseGuards(AuthGuard('jwt'))
export class AdminController {
  constructor(private readonly adminService: AdminService) {}
  
  @ApiCreatedResponse({description: 'Successfully retrieved users'})
  @ApiUnauthorizedResponse({description: 'login first'})
  @ApiForbiddenResponse({description: 'Forbidden to access this route'})
  @ApiBadRequestResponse({ description: 'Bad Request'})
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Get('users')
  findUsers(
    @Query() type: PlaceQueryDto,
    @Req() req: any
    ){
    return this.adminService.findAll(req.user, type);
  }

  @ApiOkResponse({description: 'Successfully changed the role'})
  @ApiBadRequestResponse({ description: 'Bad Request'})
  @ApiUnauthorizedResponse({description: 'login first'})
  @ApiForbiddenResponse({description: 'Forbidden to access this route'})
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Patch('/users/:email')
  changeRoles(
    @Param('email') email: string,
    @Req() req: any,
    @Body() body: ChangeRoleDTO
  ){
    return this.adminService.updateRole(req.user,email, body);
  }

  @ApiOkResponse({description: 'Successfully changed the role'})
  @ApiBadRequestResponse({ description: 'Bad Request'})
  @ApiUnauthorizedResponse({description: 'login first'})
  @ApiForbiddenResponse({description: 'Forbidden to access this route'})
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Delete('/users/:id')
  deleteUsr(
    @Param('id') id: number,
    @Req() req: any
  ){
    return this.adminService.deleteUser(req.user, id)
  }
}
