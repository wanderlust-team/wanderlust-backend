import { UserDTO } from './dto/create-user.dto';
import { Injectable, ConflictException, UnauthorizedException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { Users } from './entities/user.entity';
import * as bcrypt from 'bcrypt';
import { LoginDTO } from './dto/login.dto';
import { AdminDTO } from './dto/admin.dto';

@Injectable()
/**Class representing userServices */
export class UsersService {
  constructor(
    @InjectRepository(Users)
    private readonly userRepository: Repository<Users>,
  ) {}
  
  /**
   * @description Finds a user using their email
   * @param userEmail email to be checked against userRepo
   * @returns user object
   */
  public async findByEmail(userEmail: string): Promise<Users | null> {
    return await this.userRepository.findOne({ email: userEmail });
  }
  
  /**
   * @description the method that harshes the password
   * @param password the password to be harshed
   * @returns harshed password
   */
  private async hashPassword(password: string) : Promise<string>{
      const harshedPass = await bcrypt.hash(password, 10);
      return harshedPass;
  }
  

  /**
   * @description the method that registers a user
   * @param createUserDto the request object from the user
   * @returns created user object
   */
  public async register(createUserDto: UserDTO | AdminDTO): Promise<Users> {
    const { email, password } = createUserDto;
    const user = await this.findByEmail(email);
    if (user) throw new ConflictException('User already exists')
    const hashed = await this.hashPassword(password);
    const newUser = {...createUserDto, password: hashed}
    const created = await this.userRepository.save(newUser);
    delete created.password;
    return created
  }

    /**
   * @description this method logs in a user in the system
   * @param paramLogin email and password from a user
   * @returns user object
   */
     async findByLogin({ email, password }: LoginDTO): Promise<UserDTO> {  
      const user = await this.userRepository.findOne({ where: { email } });
      if (!user) throw new UnauthorizedException();
      const areEqual = await bcrypt.compare(password, user.password);
      
      if (!areEqual) throw new UnauthorizedException();
      delete user.password;
      return user;
  }

   /**
   * @description this method finds all the users in the system
   * @param conditions condition to be checked against userRepository
   * @returns user object
   */
   async findAllUsers(conditions): Promise<Users[]>{
     const users = await this.userRepository.find({where: conditions});
     return users.map(u=>{
       delete u.password;
       return u;
     });
   }

    /**
   * @description this method updates user roles
   * @param conditions condition to be checked against userRepository
   * @returns user object
   */
   async updateRole(email: string, newRole:  any): Promise<Users>{
     const user = await this.findByEmail(email);
     if (!user)  throw new NotFoundException('User not Found');
     user.role = newRole.role
     await this.userRepository.save(user);
     delete user.password;
     return user;
   }

   /**
 * @description finds user when passed a payload
 * @param paramPayLoad provide an email from the user payload
 * @returns user object
 */
async findByPayload({ user: { email } }: any): Promise<UserDTO> {
  const user = await this.userRepository.findOne({ 
      where:  { email } }); 
  delete user.password;
  return user;
}

   /**
 * @description deletes a user 
 * @param id user id
 * @returns user object
 */
async deleteUser(id:number): Promise<DeleteResult>{
  const user = await this.userRepository.findOne(id);
  const result = await this.userRepository.delete(user);
  if (!result.affected) throw new NotFoundException('user not found!')
  return result;
}
}
