import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsString, MinLength } from "class-validator";

export class UserDTO {

    @IsEmail()
    @IsNotEmpty()
    @ApiProperty({example: 'destinalive@gmail.com'})
    readonly email: string;

    @IsString()
    @IsNotEmpty()
    @MinLength(3, {
        message: 'FirstName should contain atleast 3 characters'
    })
    @ApiProperty({example: 'Destin'})
    readonly firstName: string;

    @IsString()
    @IsNotEmpty()
    @MinLength(3, {
        message: 'Lastname should contain atleast 3 characters'
    })
    @ApiProperty({example: 'Alive'})
    readonly lastName: string;
    
    @IsString()
    @IsNotEmpty()
    @ApiProperty({example: 'Password@3030'})
    readonly password: string
}