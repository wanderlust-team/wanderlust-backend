import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsIn, IsNotEmpty, IsString, MinLength } from "class-validator";
import { role } from "../types";

export class AdminDTO {

    @IsEmail()
    @IsNotEmpty()
    @ApiProperty({example: 'nyagatarejames@gmail.com'})
    readonly email: string;

    @IsString()
    @IsNotEmpty()
    @MinLength(3, {
        message: 'FirstName should contain atleast 3 characters'
    })
    @ApiProperty({example: 'Nyagatare'})
    readonly firstName: string;

    @IsString()
    @IsNotEmpty()
    @MinLength(3, {
        message: 'Lastname should contain atleast 3 characters'
    })
    @ApiProperty({example: 'James'})
    readonly lastName: string;
    
    @IsString()
    @IsIn(['travelAdmin', 'traveller', 'superAdmin'])
    @ApiProperty({example: 'superAdmin'})
    readonly role:role;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({example: 'Password@3030'})
    readonly password: string
}