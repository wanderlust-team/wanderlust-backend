import { ApiProperty } from "@nestjs/swagger";
import { IsIn, IsNotEmpty, IsString } from "class-validator";

export class ChangeRoleDTO{

    @IsNotEmpty()
    @IsString()
    @IsIn(['travelAdmin', 'traveller'])
    @ApiProperty({example: 'travelAdmin'})
    readonly role: string
}