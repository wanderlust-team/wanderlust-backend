import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsString} from "class-validator";

export class LoginDTO {

    @IsEmail()
    @IsNotEmpty()
    @ApiProperty({example: 'destinalive@gmail.com'})
    readonly email: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({example: 'Password@3030'})
    readonly password: string
}