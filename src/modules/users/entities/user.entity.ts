import { Place } from "../../places/entities/place.entity";
import { Column, Entity, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { role } from "../types";
import { Request } from "../../request/entities/request.entity";

@Entity('users')
export class Users {
    @PrimaryGeneratedColumn()
    id: number
    
    @Column({ nullable: false, unique: true})
    email: string

    @Column({ nullable: false})
    firstName: string

    @Column({ nullable: false})
    lastName: string

    @Column({nullable: false, default: 'traveller'})
    role: role

    @Column({ nullable: false})
    password: string

    @OneToMany(() => Place, (place) => place.owner, { onDelete: 'CASCADE' })
    places: Place;

    @OneToMany(()=> Request, (request)=> request.requestor, {onDelete: 'CASCADE'})
    requests: Request[]
  
}
