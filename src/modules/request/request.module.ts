import { Module } from '@nestjs/common';
import { RequestService } from './request.service';
import { RequestController } from './request.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Request } from './entities/request.entity';
import { PlacesModule } from '../places/places.module';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    TypeOrmModule.forFeature([Request]),
    PlacesModule,
  ],
  controllers: [RequestController],
  providers: [RequestService]
})
export class RequestModule {}
