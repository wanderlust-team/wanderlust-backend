import { Users } from '../../users/entities/user.entity';
import { PrimaryGeneratedColumn, Column, Entity, ManyToOne } from 'typeorm';
import { Place } from '../../places/entities/place.entity';

@Entity('requests')
export class Request{
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'enum',
    enum: ['PENDING', 'APPROVED', 'REJECTED'],
    nullable: false,
    default: 'PENDING',
  })
  status: 'PENDING' | 'APPROVED' | 'REJECTED';

  @Column()
  adminId: number

  @ManyToOne(() => Users, (user) => user.requests, { onDelete: 'CASCADE' })
  requestor: Users;

  @ManyToOne(() => Place, (place) => place.requests, { onDelete: 'CASCADE' })
  place?: Place;
}

