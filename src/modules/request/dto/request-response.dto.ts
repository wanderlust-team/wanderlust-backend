import { Request } from '../entities/request.entity';

export class RequestResponse {
  message: string;
  page: number;
  itemsPerPage: number;
  totalCount: number;
  data: Request[];
}
