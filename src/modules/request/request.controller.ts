import { Controller, Get, Post, Body, Patch, Param, Delete, Req, UseGuards, Query } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBadRequestResponse, ApiBearerAuth, ApiCreatedResponse, ApiForbiddenResponse, ApiInternalServerErrorResponse, ApiOkResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { PaginationDto } from '../../shared/dto/pagination.dto';
import { RequestService } from './request.service';

@ApiTags('Requests')
@ApiBearerAuth()
@Controller('requests')
@UseGuards(AuthGuard('jwt'))
export class RequestController {
  constructor(private readonly requestService: RequestService) { }

  @ApiCreatedResponse({ description: 'Successfully created a request' })
  @ApiUnauthorizedResponse({ description: 'Login first' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Post(':id')
  createRequest(
    @Req() req: any,
    @Param('id') placeId: number
  ) {
    return this.requestService.createRequest(req.user, placeId);
  } @ApiOkResponse({ description: 'Successfully found all the requests' })
  @ApiUnauthorizedResponse({ description: 'Login first' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })

  @ApiOkResponse({ description: 'Successfully found all the requests' })
  @ApiUnauthorizedResponse({ description: 'Login first' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Get('traveller')
  viewAll(
    @Req() req: any,
    @Query() paginationDto: PaginationDto
  ) {
    paginationDto.page = Number(paginationDto.page)
    paginationDto.limit = Number(paginationDto.limit)
    return this.requestService.findAllForUsers(
      req.user,
      { ...paginationDto, limit: paginationDto.limit > 10 ? 10 : paginationDto.limit }
    )
  }

  @ApiOkResponse({ description: 'Successfully found all the requests' })
  @ApiUnauthorizedResponse({ description: 'Login first' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Get('travelAdmin')
  viewRequests(
    @Req() req: any,
    @Query() paginationDto: PaginationDto
  ) {
    paginationDto.page = Number(paginationDto.page)
    paginationDto.limit = Number(paginationDto.limit)
    return this.requestService.findAllForAdmin(
      req.user,
      { ...paginationDto, limit: paginationDto.limit > 10 ? 10 : paginationDto.limit }
    );
  }

  @ApiOkResponse({ description: 'Successfully found all the requests' })
  @ApiUnauthorizedResponse({ description: 'Login first' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Patch('approve/:id')
  approveReq(
    @Param('id') reqId: number,
    @Req() req: any
  ) {
    return this.requestService.approveReq(req.user, reqId)
  }

  @ApiOkResponse({ description: 'Successfully found all the requests' })
  @ApiUnauthorizedResponse({ description: 'Login first' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Patch('reject/:id')
  rejectReq(
    @Param('id') reqId: number,
    @Req() req: any
  ) {
    return this.requestService.rejectReq(req.user, reqId)
  }
}
