import { ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Response } from '../../shared/interfaces/response.interface';
import { Repository } from 'typeorm';
import { Request } from './entities/request.entity';
import { PlacesService } from '../places/places.service';
import { PaginationDto } from '../../shared/dto/pagination.dto';
import { RequestResponse } from './dto/request-response.dto';

@Injectable()
export class RequestService {

  constructor(
    @InjectRepository(Request)
    private readonly requestRepo: Repository<Request>,
    private readonly placeService: PlacesService
  ) { }

  /**
    * @description Creates a traveller request
    * @param req contains the requester info
    * @param placeId id of the plac a requester want to visit
    * @returns Place object
    */
  async createRequest(req: any, placeId: number): Promise<Response> {
    const place = await this.placeService.findPlace(placeId);
    const adminId = place.data.owner.id;
    const newReq = this.requestRepo.create({ place: place.data, requestor: req, adminId });
    const request = await this.requestRepo.save(newReq)
    return {
      message: 'Successfully Created  request',
      data: request
    }
  }

  /**
  * @description Finds all traveller requests
  * @param req contains the requester info
  * @returns requests
  */
  async findAllForUsers(req: any, paginationDto: PaginationDto): Promise<RequestResponse> {
    const skippedItems = (paginationDto.page - 1) * paginationDto.limit;
    const [requests, totalCount] = await this.requestRepo.findAndCount({
      take: paginationDto.limit,
      skip: skippedItems,
      where: { requestor: req.id },
      relations: ['place']
    })
    if (!requests.length) throw new NotFoundException('No requests found!!');
    return {
      message: 'You successfully found all the requests',
      page: paginationDto.page,
      itemsPerPage: paginationDto.limit,
      totalCount: totalCount,
      data: requests
    }
  }

  /**
   * @description Finds all travelAdmin requests
   * @param  placeId contains the place related requests
   * @returns requests
   */
  async findAllForAdmin(req: any, paginationDto: PaginationDto): Promise<RequestResponse> {
    if (req.role !== 'travelAdmin') throw new ForbiddenException('Forbidden');
    const skippedItems = (paginationDto.page - 1) * paginationDto.limit;
    const [requests, totalCount] = await this.requestRepo.findAndCount({
      take: paginationDto.limit,
      skip: skippedItems,
      where: { adminId: req.id },
      relations: ['requestor', 'place']
    })
    if (!requests.length) throw new NotFoundException('No Requests yet');
    requests.map(req => delete req.requestor.password && delete req.adminId);
    return {
      message: 'You successfully found all the requests',
      page: paginationDto.page,
      itemsPerPage: paginationDto.limit,
      totalCount: totalCount,
      data: requests
    }
  }

  /**
   * @description Approves a request
   * @param  req contains travel admin info
   * @param id id of the request
   * @returns request
   */
  async approveReq(req: any, id: number): Promise<Response> {
    if (req.role !== 'travelAdmin') throw new ForbiddenException();
    const request = await this.requestRepo.findOne({ id }, {
      relations: ['requestor', 'place']
    })
    request.status = 'APPROVED';
    await this.requestRepo.save(request);
    delete request.requestor.password;
    return {
      message: 'You successfully approved the request',
      data: request
    }
  }

  /**
   * @description Rejects a request
   * @param  req contains travel admin info
   * @param id id of the request
   * @returns request
   */
  async rejectReq(req: any, id: number): Promise<Response> {
    if (req.role !== 'travelAdmin') throw new ForbiddenException();
    const request = await this.requestRepo.findOne({ id }, {
      relations: ['requestor', 'place']
    })
    request.status = 'REJECTED';
    await this.requestRepo.save(request);
    delete request.requestor.password;
    return {
      message: 'You successfully rejected the request',
      data: request
    }
  }

}
