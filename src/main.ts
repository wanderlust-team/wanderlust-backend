import { ValidationPipe } from '@nestjs/common';
import * as rateLimit from 'express-rate-limit';
import * as helmet from 'helmet';
import { NestFactory } from '@nestjs/core';
import { WINSTON_MODULE_NEST_PROVIDER } from 'nest-winston';
import { setUpSwaggerDocs } from './shared/config/swaggerDocs';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app.use(helmet());

  app.enableCors({
    methods: ['GET', 'POST', 'PATCH', 'DELETE']
  })

  app.use(
    rateLimit({
      windowMs: 15 * 60 * 1000, // 15 minutes
      max: 100, // limit each IP to 100 requests per windows
    }),
  );

  app.set('trust proxy', 1);

  app.setGlobalPrefix('api');

  setUpSwaggerDocs(app)

  app.useGlobalPipes(new ValidationPipe({
    whitelist: true,
    forbidNonWhitelisted: true
  }))

  app.useLogger(app.get(WINSTON_MODULE_NEST_PROVIDER))

  const configService = app.get(ConfigService)
  
  const port = configService.get('PORT') || 5000;
  await app.listen(port, ()=> console.log(`listening on port ${port}`));
}
bootstrap();
