import { Logger, Module } from '@nestjs/common';
import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
import { HttpExceptionFilter } from './shared/filter/exception.filter';
import { LoggerInteceptor } from './shared/interceptors/logger.interceptor';
import { ResponseInterceptor } from './shared/interceptors/response.interceptor';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LoggerConfig } from './shared/config/logger';
import { WinstonModule } from 'nest-winston';
import { UsersModule } from './modules/users/users.module';
import { AuthModule } from './modules/auth/auth.module';
import { PlacesModule } from './modules/places/places.module';
import { AdminModule } from './modules/admin/admin.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RequestModule } from './modules/request/request.module';
import {ConfigModule} from '@nestjs/config'
import { dbConfigService } from './shared/config/db.config';

const logger: LoggerConfig = new LoggerConfig();

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      expandVariables: true,   
    }),
    WinstonModule.forRoot(logger.console()),
    TypeOrmModule.forRootAsync({
      useClass: dbConfigService
    }),
    UsersModule,
    AuthModule,
    PlacesModule,
    AdminModule,
    RequestModule
  ],
  controllers: [AppController],
  providers: [
    Logger,
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: ResponseInterceptor
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggerInteceptor
    },
    AppService
  ],
})
export class AppModule {}
