import { 
    ExceptionFilter, 
    Catch, 
    ArgumentsHost, 
    HttpException, 
    Inject, 
    Logger, 
    LoggerService 
  } from '@nestjs/common';
  import { Request, Response } from 'express';
  
  @Catch(HttpException)
  export class HttpExceptionFilter implements ExceptionFilter {
    constructor(@Inject(Logger) private readonly loggerService: LoggerService){}
    catch(exception: HttpException, host: ArgumentsHost) {
      const ctx = host.switchToHttp();
      const response = ctx.getResponse<Response>();
      const request = ctx.getRequest<Request>();
      const status = exception.getStatus();
      const message = exception.getResponse();
  
      this.loggerService.verbose(`REQUEST: ${request.method} ${request.url} `);
      this.loggerService.error(exception.stack);
  
      response
        .status(status)
        .json({
          error: message,
          timestamp: new Date().toISOString(),
          path: request.url,
        });
    }
  }