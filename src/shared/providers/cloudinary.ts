import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { v2 as cloudinary } from 'cloudinary';
import { config } from 'dotenv'


@Injectable()
export default class CloudinaryService {
  constructor (private configService: ConfigService){}
  /**
   * @description this methods uploads a file to cloudinary
   * @param file the file 
   * @returns 
   */
  
  async uplaodToCloud(file: any): Promise<any>{
    cloudinary.config({
      cloud_name: this.configService.get('CLOUDINARY_NAME'),
      api_key: this.configService.get('CLOUDINARY_API_KEY'),
      api_secret: this.configService.get('CLOUDINARY_API_SECRET'),
    });
    
      return new Promise((resolve, reject) => {
        cloudinary.uploader.upload(file, (err, result) => {
          if (err) return reject(err);
          return resolve(result.url);
        });
      })  
  }
}
