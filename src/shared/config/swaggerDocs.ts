import { NestExpressApplication } from "@nestjs/platform-express";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";

const options = new DocumentBuilder()
  .addBearerAuth()
  .setTitle('WanderLust')
  .setDescription('WanderLust app api description')
  .setVersion('1.0.0')
  .build();

  export const setUpSwaggerDocs= (app: NestExpressApplication) =>{
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('docs', app, document)
  }
  