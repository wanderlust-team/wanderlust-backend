import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from "@nestjs/typeorm";
import { Request } from "../../modules/request/entities/request.entity";
import { Place } from "../../modules/places/entities/place.entity";
import { Users } from '../../modules/users/entities/user.entity';
import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";

@Injectable()
export class dbConfigService implements TypeOrmOptionsFactory {
    constructor(private configService: ConfigService) { }
    createTypeOrmOptions(): TypeOrmModuleOptions {
        return {
            type: 'postgres',
            host: this.configService.get('HOST'),
            port: 5432,
            username: this.configService.get('DB_USER_NAME'),
            password: this.configService.get('DB_PASSWORD'),
            database: this.configService.get('NODE_ENV') === 'test'
                ? this.configService.get('DB_TEST') : this.configService.get('DB_NAME'),
            entities: [Users, Place, Request],
            synchronize: this.configService.get('NODE_ENV') !== 'production',
            migrations: ['dist/src/db/migrations/*.js'],
            cli: {
                migrationsDir: 'src/db/migrations',
            },
            ssl:
                this.configService.get('NODE_ENV') === 'production'
                    ? {
                        rejectUnauthorized: false,
                    }
                    : false,

            autoLoadEntities: true,
            logging: this.configService.get('NODE_ENV') !== 'production' && this.configService.get('NODE_ENV') !== 'test',

        }
    }
}
