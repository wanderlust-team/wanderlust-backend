import { Place } from "../../modules/places/entities/place.entity";
import { Users } from "../../modules/users/entities/user.entity";

export interface Response{
    message: string,
    data: Users | Users[] | Place|any  
}