import * as request from 'supertest';
import { INestApplication } from "@nestjs/common";
import { Test } from "@nestjs/testing";
import { Connection, getConnection } from "typeorm";
import { AppModule } from '../src/app.module';
import mockData from './mockData';
import * as fs from 'fs';
import * as path from 'path';

let app: INestApplication;
let connection: Connection;
describe('Request', ()=>{
    beforeAll(async ()=>{
        jest.setTimeout(30000);
        const module = await Test.createTestingModule({
            imports: [
                AppModule, 
            ]
        }).compile();
        app = module.createNestApplication();
        await app.init();
        connection = getConnection();
        await connection.synchronize(true)
        
        const img = path.join(__dirname, './pics/virunga.jpg'); 
        await request(app.getHttpServer()).post('/auth/signup/admin/secure').send(mockData.adminSign)
        const adminSign = await request(app.getHttpServer()).post('/auth/login').send(mockData.adminLog)
        mockData.adminToken= adminSign.body.data.accessToken
        await request(app.getHttpServer()).post('/auth/signup').send(mockData.signup)
        await request(app.getHttpServer()).patch('/admin/users/jamesnyagatare@gmail.com').set('Authorization', `Bearer ${mockData.adminToken}`).send(mockData.updateRole)
        const travelAdLog =  await request(app.getHttpServer()).post('/auth/login').send(mockData.login);
        mockData.travelAdmToken = travelAdLog.body.data.accessToken;
        await request(app.getHttpServer()).post('/auth/signup').send(mockData.signUpSec)
        const travellerLog =  await request(app.getHttpServer()).post('/auth/login').send(mockData.travLogin);
        mockData.travellerToken = travellerLog.body.data.accessToken
        const place = await request(app.getHttpServer()).post('/places').set('Authorization', `Bearer ${mockData.travelAdmToken}`).field(mockData.place)
          .attach('file', fs.readFileSync(img), 'virunga.jpg');
       mockData.placeId = place.body.data.id;
       const travelReq = await request(app.getHttpServer()).post(`/requests/${mockData.placeId}`).set('Authorization', `Bearer ${mockData.travellerToken}`);
       mockData.reqId = travelReq.body.data.id;
    })

    afterAll(async ()=>{
         await connection.synchronize(true)
        await app.close();
    })

    it(`/POST Traveller should be able to create a request`, () => {
        return request(app.getHttpServer())
          .post(`/requests/${mockData.placeId}`)
          .set('Authorization', `Bearer ${mockData.travellerToken}`)   
    });
    
    it(`/GET Traveller should be able to view all the requests created`, ()=>{
        return request(app.getHttpServer())
        .get('/requests/traveller')
        .set('Authorization', `Bearer ${mockData.travellerToken}`)
    })
})
