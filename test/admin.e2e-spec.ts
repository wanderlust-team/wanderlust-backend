import * as request from 'supertest';
import { INestApplication } from "@nestjs/common";
import { Test } from "@nestjs/testing";
import { Users} from "../src/modules/users/entities/user.entity";
import { Repository } from "typeorm";
import { AppModule } from '../src/app.module';
import mockData from './mockData';

let app: INestApplication;
let userRepository: Repository<Users>
describe('Admin', ()=>{
    beforeAll(async ()=>{
        const module = await Test.createTestingModule({
            imports: [
                AppModule, 
            ]
        }).compile();
        app = module.createNestApplication();
        app.init();
        userRepository = module.get('UsersRepository');
        await userRepository.query(`DELETE FROM users;`);
        await request(app.getHttpServer()).post('/auth/signup/admin/secure').send(mockData.adminSign)
        const user = await request(app.getHttpServer()).post('/auth/signup').send(mockData.signup)
        const adminSign = await request(app.getHttpServer()).post('/auth/login').send(mockData.adminLog)
        mockData.adminToken= adminSign.body.data.accessToken
        mockData.userId= user.body.data.id
    })
    
    afterAll(async ()=>{
        await userRepository.query(`DELETE FROM users;`);
        await app.close();
    })
    
    it(`/GET Admin should be able to see all users`, () => {
        return request(app.getHttpServer())
          .get('/admin/users?type=users')
          .set('Authorization', `Bearer ${mockData.adminToken}`)
          .expect(200);
      });

    it(`/PATCH Admin should be able to change user's role`, ()=>{
        return request(app.getHttpServer())
          .patch('/admin/users/jamesnyagatare@gmail.com')
          .set('Authorization', `Bearer ${mockData.adminToken}`)
          .expect(200)
    })

    it(`/DELETE Admin should be able to delete a user`, ()=>{
        return request(app.getHttpServer())
          .delete(`/admin/users/${mockData.userId}`)
          .set('Authorization', `Bearer ${mockData.adminToken}`)
          .expect(200)
    })
})
