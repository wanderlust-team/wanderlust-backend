import * as request from 'supertest';
import { INestApplication } from "@nestjs/common";
import { Test } from "@nestjs/testing";
import { Users} from "../src/modules/users/entities/user.entity";
import { Repository } from "typeorm";
import { AppModule } from '../src/app.module';
import mockData from './mockData';

let app: INestApplication;
let userRepository: Repository<Users>
describe('Auth', ()=>{
    beforeAll(async ()=>{
        const module = await Test.createTestingModule({
            imports: [
                AppModule, 
            ]
        }).compile();
        app = module.createNestApplication();
        app.init();
        userRepository = module.get('UsersRepository');
        await userRepository.query(`DELETE FROM users;`);
    })
    
    afterAll(async ()=>{
        await userRepository.query(`DELETE FROM users;`);
        await app.close();
    })
    
    it(`/POST signup`, () => {
        return request(app.getHttpServer())
          .post('/auth/signup')
          .send(mockData.signup)
          .expect(201);
      });

      it(`/POST signupAdd`, ()=>{
          return request(app.getHttpServer())
          .post('/auth/signup/admin/secure')
          .send(mockData.adminSign)
          .expect(201)
      });
      
    it(`/POST login`, ()=>{
        return request(app.getHttpServer())
          .post('/auth/login')
          .send(mockData.login)
          .expect(200);
    });

    
})
