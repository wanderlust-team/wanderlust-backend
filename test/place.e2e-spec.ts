import * as request from 'supertest';
import { INestApplication } from "@nestjs/common";
import { Test } from "@nestjs/testing";
import { Connection, getConnection } from "typeorm";
import { AppModule } from '../src/app.module';
import mockData from './mockData';
import * as fs from 'fs';
import * as path from 'path';

let app: INestApplication;
let connection: Connection;
describe('Place', ()=>{
    beforeAll(async ()=>{
        const module = await Test.createTestingModule({
            imports: [
                AppModule, 
            ]
        }).compile();
        app = module.createNestApplication();
        await app.init();
        connection = getConnection();
        await connection.synchronize(true)

        await request(app.getHttpServer()).post('/auth/signup/admin/secure').send(mockData.adminSign)
        const adminSign = await request(app.getHttpServer()).post('/auth/login').send(mockData.adminLog)
        mockData.adminToken= adminSign.body.data.accessToken
        await request(app.getHttpServer()).post('/auth/signup').send(mockData.signup)
        await request(app.getHttpServer()).patch('/admin/users/jamesnyagatare@gmail.com').set('Authorization', `Bearer ${mockData.adminToken}`).send(mockData.updateRole)
        const travelAdLog =  await request(app.getHttpServer()).post('/auth/login').send(mockData.login);
        mockData.travelAdmToken = travelAdLog.body.data.accessToken
    })

    afterAll(async ()=>{
         await connection.synchronize(true)
        await app.close();
    })
    
    it(`/POST Travel admin should be able to create a place`, () => {
        const img = path.join(__dirname, './pics/virunga.jpg'); 
        jest.setTimeout(30000);
        
        return request(app.getHttpServer())
          .post('/places')
          .set('Authorization', `Bearer ${mockData.travelAdmToken}`)
          .field(mockData.place)
          .attach('file', fs.readFileSync(img), 'virunga.jpg')
          .expect(201);
      });

      it(`/GET all available places`, ()=>{
          return request(app.getHttpServer()).get('/places').set('Authorization', `Bearer ${mockData.travelAdmToken}`)
            .expect(200)
      })
})
