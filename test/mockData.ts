export default{
  signup: {
    firstName: 'James',
    lastName: 'Nyagatare',
    email: 'jamesnyagatare@gmail.com',
    password: 'Password',
  },
  signUpSec:{
    firstName: 'Bienaime',
    lastName: 'Byiringiro',
    email: 'Bybaime@gmail.com',
    password: 'Password',
  },
  adminSign: {
    firstName: 'Issa',
    lastName: 'Jean Marie',
    email: 'jeanmarieissa@gmail.com',
    password: 'Password',
    role: 'superAdmin'
  },
  adminLog:{
    email: 'jeanmarieissa@gmail.com',
    password: 'Password',
  },
  login:{
    email: 'jamesnyagatare@gmail.com',
    password: 'Password',
  },
  travLogin:{
    email: 'Bybaime@gmail.com',
    password: 'Password',
  },
  place:{
    title: 'Virunga national Park',
    description: 'Virunga National Park is a national park in the Albertine Rift Valley in the eastern part of the Democratic Republic of the Congo. It was created in 1925 and is among the first protected areas in Africa. In elevation, it ranges from 680 m in the Semliki River valley to 5,109 m in the Rwenzori Mountains',
    price: 20000,
  },
  updateRole:{
    role: 'travelAdmin'
  },
  adminToken: '',
  userId: '',
  travelAdmToken: '',
  placeId: '',
  travellerToken: '',
  reqId: ''
}